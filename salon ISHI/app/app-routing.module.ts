 import { NgModule } from '@angular/core';
 import {RouterModule, Routes} from '@angular/router';
 import { AboutUsComponent } from './about-us/about-us.component';
 import { HomeComponent } from './home/home.component';
 import { ContactUsComponent } from './contact-us/contact-us.component';
 import { GalleryComponent } from './gallery/gallery.component';
 import { ServicesComponent } from './services/services.component';
 import { HairBComponent } from './hair-b/hair-b.component';
 import { NailsComponent } from './nails/nails.component';
 import { DressingsComponent } from './dressings/dressings.component';

 const routes: Routes = [
   { path: '', component: HomeComponent},
   { path: 'about-us', component: AboutUsComponent},
   { path: 'services', component: ServicesComponent},
   { path: 'gallery', component: GalleryComponent},
   { path: 'contact-us', component: ContactUsComponent},
   { path: 'hair-b', component: HairBComponent},
   { path: 'nails', component: NailsComponent},
   { path: 'dressings', component: DressingsComponent},
   ];

 @NgModule({
  imports: [RouterModule.forRoot(routes)],
   exports: [RouterModule]
 })
 export class AppRoutingModule {}
 // tslint:disable-next-line:max-line-length
 export const routingComponents = [AboutUsComponent, ServicesComponent , GalleryComponent, ContactUsComponent , HairBComponent , NailsComponent , DressingsComponent ];

