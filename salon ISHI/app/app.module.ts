import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
// import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { GalleryComponent } from './gallery/gallery.component';
// import { ServicesComponent } from './services/services.component';
import {AppRoutingModule, routingComponents} from './app-routing.module';
import {MatTableModule} from '@angular/material/table';
import {Sort} from '@angular/material/sort';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatIconModule} from "@angular/material/icon";
import { HairBComponent } from './hair-b/hair-b.component';
import { NailsComponent } from './nails/nails.component';
import { DressingsComponent } from './dressings/dressings.component';
import {MatCardModule} from "@angular/material/card";


@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HomeComponent,
    ContactUsComponent,
    GalleryComponent,
    routingComponents,
    HairBComponent,
    NailsComponent,
    DressingsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatFormFieldModule,
    MatIconModule,
    MatCardModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
