import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DressingsComponent } from './dressings.component';

describe('DressingsComponent', () => {
  let component: DressingsComponent;
  let fixture: ComponentFixture<DressingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DressingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DressingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
