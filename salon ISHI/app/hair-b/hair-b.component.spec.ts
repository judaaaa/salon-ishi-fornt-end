import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HairBComponent } from './hair-b.component';

describe('HairBComponent', () => {
  let component: HairBComponent;
  let fixture: ComponentFixture<HairBComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HairBComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HairBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
